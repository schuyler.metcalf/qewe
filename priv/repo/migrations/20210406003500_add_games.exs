defmodule Qewe.Repo.Migrations.AddGames do
  use Ecto.Migration

  def change do
    create table(:games, primary_key: false) do
      add :uuid, :uuid, primary_key: true
      add :slug, :text

      timestamps()
    end

    create unique_index(:games, [:slug])
  end
end
