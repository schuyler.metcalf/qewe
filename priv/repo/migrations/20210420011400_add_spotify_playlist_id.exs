defmodule Qewe.Repo.Migrations.AddSpotifyPlaylistId do
  use Ecto.Migration

  def change do
    alter table(:games) do
      add :spotify_playlist_uuid, :string
    end
  end
end
