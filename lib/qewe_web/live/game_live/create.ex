defmodule QeweWeb.GameLive.Create do
  use QeweWeb, :live_view
  alias Qewe.Games
  alias Qewe.Support.Slugs
  alias Qewe.Games.Commands.CreateGame

  require Logger
  @impl true
  def mount(_params, session, socket) do
    session_id = Map.get(session, "spotify_session_id")

    socket =
      assign(socket, :session_id, session_id)
      |> assign(:changeset, Games.change_game())
      |> assign(:create_game_event, CreateGame.new())

    Logger.info("After Mount: #{inspect(socket)}")
    {:ok, socket}
  end

  @impl true
  def handle_event("refresh_slug", _, socket) do
    {:noreply, update(socket, :changeset, fn cset -> update_slug(cset) end)}
  end

  def handle_event("save", %{"create_game" => game_params}, socket) do
    Logger.info("Saving game with params #{inspect(game_params)}")

    game_params
    |> Map.put("session_id", Map.get(socket.assigns, :session_id))
    |> save_game(socket)
  end

  def handle_event("validate", _, socket) do
    {:noreply, socket}
  end

  defp update_slug(changeset) do
    Ecto.Changeset.change(changeset, %{slug: Slugs.create()})
  end

  defp save_game(game_params, socket) do
    case Games.create_game(game_params) do
      {:ok, _game} ->
        {:noreply,
         socket
         |> put_flash(:info, "Game created successfully")
         |> push_redirect(to: Routes.game_index_path(socket, :index))}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end
end
