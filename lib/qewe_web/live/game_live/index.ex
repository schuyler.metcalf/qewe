defmodule QeweWeb.GameLive.Index do
  use QeweWeb, :live_view

  alias Qewe.Games
  alias Qewe.Games.Projections.Game
  alias Qewe.Games.Commands.CreateGame
  require Logger

  @impl true
  def mount(_params, session, socket) do
    socket =
      assign(socket, :games, list_games())
      |> assign(:session_id, Map.get(session, "spotify_session_id"))

    {:ok, socket}
  end

  @impl true
  def handle_params(params, _url, socket) do
    {:noreply, apply_action(socket, socket.assigns.live_action, params)}
  end

  @impl true
  def handle_event("spotify-login", _, socket) do
    {:noreply, redirect(socket, external: Qewe.Qotify.auth_url())}
  end

  # defp apply_action(socket, :edit, %{"id" => id}) do
  #   socket
  #   |> assign(:page_title, "Edit Game")
  #   |> assign(:game, Games.get_game!(id))
  # end

  defp apply_action(socket, :new, _params) do
    socket
    |> assign(:page_title, "New Game")
    |> assign(:create_game, %CreateGame{})
  end

  defp apply_action(socket, :index, _params) do
    socket
    |> assign(:page_title, "Listing Games")
    |> assign(:create_game, nil)
  end

  # @impl true
  # def handle_event("delete", %{"id" => id}, socket) do
  #   game = Games.get_game!(id)
  #   {:ok, _} = Games.delete_game(game)

  #   {:noreply, assign(socket, :games, list_games())}
  # end

  defp list_games do
    Games.list_games()
  end
end
