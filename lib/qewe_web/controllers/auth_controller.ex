defmodule QeweWeb.AuthController do
  use QeweWeb, :controller
  require Logger

  def index(conn, %{"code" => code}) do
    Logger.info("Recieved a response from spotify.......")

    case Qewe.Qotify.Sessions.create_session(code) do
      {:ok, auth} ->
        conn = Plug.Conn.put_session(conn, :spotify_session_id, auth)
        redirect(conn, to: Routes.game_index_path(conn, :index))
    end
  end
end
