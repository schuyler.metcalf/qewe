defmodule Qewe.Support.Middleware.ValidationMiddleware do
  @behaviour Commanded.Middleware

  alias Commanded.Middleware.Pipeline

  def before_dispatch(%Pipeline{command: command} = pipeline) do
    with :ok <- command.__struct__.valid?(command) do
      pipeline
    else
      {:error, msgs} ->
        pipeline
        |> Pipeline.respond({:error, :command_validation_failure, command, msgs})
        |> Pipeline.halt()
    end
  end

  def after_dispatch(pipeline), do: pipeline
  def after_failure(pipeline), do: pipeline
end
