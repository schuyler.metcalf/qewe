defmodule Qewe.Support.Slugs do
  def create(size \\ 3) do
    MnemonicSlugs.generate_slug(size)
  end
end
