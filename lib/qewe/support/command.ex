defmodule Qewe.Support.Command do
  @callback handle_validate(Ecto.Changeset.t()) :: Ecto.Changeset.t()
  @callback schema() :: map()

  defmacro __using__(schema) do
    quote do
      use Ecto.Schema

      import Ecto.Changeset
      @behaviour unquote(__MODULE__)

      require Logger

      embedded_schema do
        Enum.map(unquote(schema), fn
          {name, type} -> field(name, type)
        end)
      end

      @cast_keys unquote(schema) |> Enum.into(%{}) |> Map.keys()

      def changeset(%__MODULE__{} = evt, args) do
        Ecto.Changeset.cast(evt, args, @cast_keys)
      end

      def changeset(args) do
        changeset(%__MODULE__{}, args)
      end

      def new(args \\ %{}) do
        args
        |> changeset()
        |> Ecto.Changeset.apply_action!(:create)
      end

      def handle_validate(changeset), do: handle_validate(changeset, [])
      def handle_validate(changeset, _opts), do: changeset

      def schema(), do: %{}

      def valid?(data) do
        data
        |> Map.from_struct()
        |> Skooma.valid?(schema())
      end

      defoverridable handle_validate: 1, handle_validate: 2, schema: 0
    end
  end
end
