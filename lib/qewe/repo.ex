defmodule Qewe.Repo do
  use Ecto.Repo,
    otp_app: :qewe,
    adapter: Ecto.Adapters.Postgres
end
