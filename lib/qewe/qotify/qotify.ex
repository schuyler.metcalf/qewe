defmodule Qewe.Qotify do
  defmodule __MODULE__.Tokens do
    defstruct [:access_token, :refresh_token]
  end

  alias __MODULE__.Tokens
  alias Qewe.Qotify.Sessions
  alias Qewe.Qotify.Sessions.Session
  alias Qewe.Qotify.Sessions.UserInfo

  require Logger

  def auth_url() do
    Spotify.Authorization.url()
  end

  def authenticate(auth_code) do
    with {:ok, auth} <-
           Spotify.Authentication.authenticate(%Spotify.Credentials{}, %{"code" => auth_code}) do
      {:ok, %Tokens{access_token: auth.access_token, refresh_token: auth.refresh_token}}
    else
      result -> result
    end
  end

  def current_user(credentials) do
    creds =
      credentials
      |> as_spotify_ex_credentials()

    with {:ok, %Spotify.Profile{} = user} <- Spotify.Profile.me(creds) do
      new_user =
        user
        |> make_user()

      {:ok, new_user}
    end
  end

  def create_playlist(session_id, %{name: name}) do
    %Session{credentials: creds, user_info: user_info} = Sessions.get!(session_id)
    spotify_creds = as_spotify_ex_credentials(creds)
    body = "{\"name\": \"#{name}\"}"

    with {:ok, playlist} <-
           Spotify.Playlist.create_playlist(spotify_creds, user_info.spotify_user_id, body) do
      {:ok, playlist.id}
    else
      result -> result
    end
  end

  defp as_spotify_ex_credentials(%Tokens{access_token: access_token, refresh_token: refresh_token}) do
    %Spotify.Credentials{access_token: access_token, refresh_token: refresh_token}
  end

  defp as_spotify_ex_credentials(%Session{credentials: creds}) do
    as_spotify_ex_credentials(creds)
  end

  defp make_user(%Spotify.Profile{} = profile) do
    %UserInfo{
      spotify_user_id: profile.id,
      display_name: profile.display_name,
      profile_photo_url: extract_photo_url(profile)
    }
  end

  defp extract_photo_url(%Spotify.Profile{images: [head | _]}) do
    Map.get(head, "url")
  end
end
