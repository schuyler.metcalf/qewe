defmodule Qewe.Qotify.Sessions.UserInfo do
  defstruct [:display_name, :profile_photo_url, :spotify_user_id]
end
