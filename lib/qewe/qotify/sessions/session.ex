defmodule Qewe.Qotify.Sessions.Session do
  defstruct session_id: nil,
            credentials: nil,
            user_info: nil
end
