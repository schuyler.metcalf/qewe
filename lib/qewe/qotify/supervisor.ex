defmodule Qewe.Qotify.Supervisor do
  use Supervisor

  @impl true
  def init(_) do
    children = [
      {Registry, [name: Qewe.Qotify.Registry, keys: :unique]},
      {Qewe.Qotify.Sessions.Supervisor, strategy: :one_for_one}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end

  def start_link(args) do
    Supervisor.start_link(__MODULE__, args)
  end
end
