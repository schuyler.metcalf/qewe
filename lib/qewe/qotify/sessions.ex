defmodule Qewe.Qotify.Sessions do
  use GenServer

  alias Qewe.Qotify
  alias Qewe.Qotify.Sessions.Session

  require Logger

  def create_session(auth_code) do
    with {:ok, credentials} <- Qotify.authenticate(auth_code) do
      {:ok, user} = Qotify.current_user(credentials)
      id = UUID.uuid4()

      DynamicSupervisor.start_child(
        Qewe.Qotify.Sessions.Supervisor,
        {__MODULE__, {id, %Session{session_id: id, user_info: user, credentials: credentials}}}
      )

      Logger.info("Creating session with id #{inspect(id)}")
      {:ok, id}
    else
      {:error, msg} ->
        {:error, "Unable to authenticate with Spotify for the following reasons: ", msg}
    end
  end

  def get!(id) when is_nil(id) do
    raise("ID must be defined")
  end

  def get!(id) do
    GenServer.call(via(id), :get)
  end

  @impl true
  def init({id, session}) do
    {:ok, {id, session}}
  end

  def child_spec({id, session}) do
    %{
      id: {__MODULE__, id},
      start: {__MODULE__, :start_link, [{id, session}]},
      restart: :temporary
    }
  end

  def start_link({id, session}) do
    GenServer.start_link(__MODULE__, {id, session}, name: via(id))
  end

  defp via(id) do
    {:via, Registry, {Qewe.Qotify.Registry, id}}
  end

  @impl true
  def handle_call(:get, _from, {id, session}) do
    {:reply, session, {id, session}}
  end
end
