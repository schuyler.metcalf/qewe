defmodule Qewe.App do
  use Commanded.Application,
    otp_app: :qewe,
    event_store: [
      adapter: Commanded.EventStore.Adapters.EventStore,
      event_store: Qewe.EventStore
    ]

  router(Qewe.Router)
end
