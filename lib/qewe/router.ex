defmodule Qewe.Router do
  use Commanded.Commands.Router

  alias Qewe.Games.Commands.CreateGame
  alias Qewe.Games.Commands.AddSong

  alias Qewe.Games.Aggregates.Game

  alias Qewe.Sessions.Aggregates.Session

  middleware(Qewe.Support.Middleware.ValidationMiddleware)

  identify(Game, by: :game_uuid)
  identify(Session, by: :session_uuid)

  dispatch([CreateGame, AddSong], to: Game)
end
