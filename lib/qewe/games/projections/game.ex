defmodule Qewe.Games.Projections.Game do
  use Ecto.Schema

  @derive {Phoenix.Param, key: :slug}
  @primary_key {:uuid, :binary_id, autogenerate: false}

  schema "games" do
    field(:slug, :string)
    field(:spotify_playlist_uuid, :string)

    timestamps()
  end
end
