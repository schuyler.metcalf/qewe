defmodule Qewe.Games do
  alias Qewe.App

  alias Qewe.Games.Projections.Game

  alias Qewe.Games.Commands.CreateGame
  alias Qewe.Games.Commands.AddSong
  alias Qewe.Repo
  alias Qewe.Support.Slugs

  require Logger

  def create_game(attrs \\ %{}) do
    uuid = UUID.uuid4()

    create_game =
      attrs
      |> CreateGame.new()
      |> CreateGame.assign_uuid(uuid)

    Logger.info("Attempting to save command #{inspect(create_game)}")

    with :ok <- App.dispatch(create_game, consistency: :strong) do
      {:ok, uuid}
    else
      reply -> reply
    end
  end

  def add_song(game_id, attrs \\ %{}) do
    uuid = UUID.uuid4()

    enqueue_song =
      attrs
      |> AddSong.new()
      |> AddSong.assign_uuid(uuid)
      |> AddSong.assign_game_uuid(game_id)

    with :ok <- App.dispatch(enqueue_song) do
      {:ok, uuid}
    else
      reply -> reply
    end
  end

  def get_game_by_slug!(slug) do
    Repo.get_by!(Game, slug: slug)
  end

  def list_games() do
    Repo.all(Game)
  end

  defp get(schema, uuid) do
    case Repo.get(schema, uuid) do
      nil -> {:error, :not_found}
      projection -> {:ok, projection}
    end
  end

  def change_game(args) when is_map(args) do
    args
    |> CreateGame.changeset()
    |> CreateGame.handle_validate()
  end

  def change_game(%CreateGame{} = create, args \\ %{}) do
    create
    |> CreateGame.changeset(args)
    |> CreateGame.handle_validate()
  end

  def change_game() do
    CreateGame.new()
    |> CreateGame.assign_slug(new_slug())
    |> change_game(%{})
  end

  defp new_slug() do
    Slugs.create()
  end
end
