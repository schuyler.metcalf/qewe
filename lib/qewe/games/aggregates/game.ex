defmodule Qewe.Games.Aggregates.Game do
  defstruct uuid: nil,
            slug: nil,
            session_id: nil

  alias Qewe.Games.Aggregates.Game
  alias Qewe.Games.Commands.CreateGame
  alias Qewe.Games.Commands.AddSong

  alias Qewe.Games.Events.GameCreated
  alias Qewe.Games.Events.SongAdded

  def execute(%Game{} = _game, %CreateGame{} = create) do
    %GameCreated{
      game_uuid: create.game_uuid,
      slug: create.slug,
      session_id: create.session_id
    }
  end

  def execute(%Game{} = _game, %AddSong{} = add) do
    %SongAdded{game_uuid: add.game_uuid, track_id: add.track_id}
  end

  def apply(%Game{} = game, %GameCreated{} = created) do
    %Game{game | uuid: created.game_uuid, slug: created.slug, session_id: created.session_id}
  end

  def apply(%Game{} = game, %SongAdded{} = _added) do
    game
  end
end
