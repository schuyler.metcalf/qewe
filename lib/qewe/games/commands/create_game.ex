defmodule Qewe.Games.Commands.CreateGame do
  # defstruct game_uuid: "",
  #           slug: ""

  use Qewe.Support.Command, game_uuid: :binary_id, slug: :string, session_id: :binary_id
  alias Qewe.Games.Commands.CreateGame

  def assign_uuid(%CreateGame{} = game, uuid) do
    %CreateGame{game | game_uuid: uuid}
  end

  def assign_slug(%CreateGame{} = game, slug) do
    %CreateGame{game | slug: slug}
  end

  @impl true
  def handle_validate(changeset) do
    changeset
    |> validate_required([:slug])
  end

  @impl true
  def schema() do
    %{game_uuid: :string, slug: :string, session_id: :string}
  end
end
