defmodule Qewe.Games.Commands.AddSong do
  use Qewe.Support.Command,
    uuid: :binary_id,
    game_uuid: :binary_id,
    track_id: :string,
    session_id: :binary_id

  alias Qewe.Games.Commands.AddSong

  def assign_uuid(cmd, uuid) do
    %AddSong{cmd | uuid: uuid}
  end

  def assign_game_uuid(cmd, uuid) do
    %AddSong{cmd | game_uuid: uuid}
  end
end
