defmodule Qewe.Games.Supervisor do
  use Supervisor

  alias Qewe.Games

  def start_link(arg) do
    Supervisor.start_link(__MODULE__, arg, name: __MODULE__)
  end

  def init(_arg) do
    Supervisor.init(
      [Games.Projectors.GamesProjector],
      strategy: :one_for_one
    )
  end
end
