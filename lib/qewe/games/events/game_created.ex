defmodule Qewe.Games.Events.GameCreated do
  @derive Jason.Encoder
  defstruct [
    :game_uuid,
    :slug,
    :spotify_user_id,
    :session_id
  ]
end
