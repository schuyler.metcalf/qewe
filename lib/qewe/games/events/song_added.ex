defmodule Qewe.Games.Events.SongAdded do
  @derive Jason.Encoder
  defstruct [
    :game_uuid,
    :track_id
  ]
end
