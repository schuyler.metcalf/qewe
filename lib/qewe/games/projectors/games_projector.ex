defmodule Qewe.Games.Projectors.GamesProjector do
  use Commanded.Projections.Ecto,
    application: Qewe.App,
    repo: Qewe.Repo,
    name: "games_projector"

  alias Qewe.Games.Events.GameCreated
  alias Qewe.Games.Projections.Game

  project(
    %GameCreated{game_uuid: uuid, slug: slug, session_id: session_id},
    _meta,
    fn multi ->
      {:ok, playlist_uuid} =
        Qewe.Qotify.create_playlist(session_id, %{
          name: "Qewe playlist for #{slug}"
        })

      Ecto.Multi.insert(multi, :game_created, %Game{
        uuid: uuid,
        slug: slug,
        spotify_playlist_uuid: playlist_uuid
      })
    end
  )
end
