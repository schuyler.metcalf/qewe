defmodule Qewe.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      Qewe.App,

      # Start the Ecto repository
      Qewe.Repo,
      # Start the Telemetry supervisor
      QeweWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Qewe.PubSub},
      # Start the Endpoint (http/https)
      QeweWeb.Endpoint,
      # Start a worker by calling: Qewe.Worker.start_link(arg)
      # {Qewe.Worker, arg}
      Qewe.Games.Supervisor,
      Qewe.Qotify.Supervisor
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Qewe.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    QeweWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
