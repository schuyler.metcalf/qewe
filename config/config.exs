# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :qewe,
  ecto_repos: [Qewe.Repo]

# Configures the endpoint
config :qewe, QeweWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "bY21vLNbc+2uZy74zb+9BjMgbUqrkXI7hrspdD1HD2SRKuOg0zIHzUrOo05f5Kp4",
  render_errors: [view: QeweWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Qewe.PubSub,
  live_view: [signing_salt: "7wbu6zRQ"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :qewe, event_stores: [Qewe.EventStore]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
import_config "spotify.exs"
import_config "spotify.secret.exs"
