use Mix.Config

config :spotify_ex,
  scopes: ["playlist-modify-private", "playlist-modify-public"],
  callback_url: "http://localhost:4000/auth-callback"
