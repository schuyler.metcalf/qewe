defmodule Qewe.Storage do
  def reset!(tags) do
    {:ok, _apps} = Application.ensure_all_started(:qewe)

    reset_readstore(tags)
  end

  defp reset_readstore(tags) do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Qewe.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Qewe.Repo, {:shared, self()})
    end
  end

  def reset_eventstore() do
    :ok = Application.stop(:qewe)
  end
end
