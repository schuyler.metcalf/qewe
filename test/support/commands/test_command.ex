defmodule Qewe.Support.TestCommand do
  use Qewe.Support.Command, value: :string

  require Logger

  defp value_valid?(val) do
    # fn _ ->
    if String.equivalent?(val, "invalid") do
      {:error, "Value is invalid!"}
    else
      :ok
    end

    # end
  end

  @impl true
  def schema() do
    %{value: [:string, &value_valid?/1]}
  end
end
