defmodule Qewe.Support.Projectors do
  alias Qewe.Repo
  import Ecto.Query, only: [from: 2]

  def get_last_seen_event_number(name) do
    from(
      p in "projection_versions",
      where: p.projection_name == ^name,
      select: p.last_seen_event_number
    )
    |> Repo.one() || 0
  end

  def only_instance_of(module, preloads \\ []) do
    module |> Repo.one!() |> Repo.preload(preloads)
  end
end
