defmodule Qewe.Qotify.SessionsTest do
  use Qewe.DataCase

  alias Qewe.Qotify.Sessions
  alias Qewe.Qotify.Sessions.Session
  alias Qewe.Qotify.Tokens
  alias Qewe.Qotify.Sessions.UserInfo

  import Mock

  @userinfo %UserInfo{}
  @tokens %Tokens{access_token: "12345", refresh_token: "54321"}

  describe "it creates a new session" do
    test_with_mock "it creates a new session when Spotify is successful", _, Qewe.Qotify, [],
      authenticate: fn _code -> {:ok, @tokens} end,
      current_user: fn _cred -> {:ok, @userinfo} end do
      good_code = "123456"

      assert {:ok, id} = Sessions.create_session(good_code)

      assert %Session{credentials: %Tokens{} = creds, user_info: %UserInfo{} = userinfo} =
               Sessions.get!(id)

      assert userinfo == @userinfo
      assert creds == @tokens
    end
  end
end
