defmodule Qewe.Games.Projectors.GamesTest do
  use Qewe.DataCase

  alias Qewe.Games.Events.GameCreated
  alias Qewe.Games.Projections.Game
  alias Qewe.Games.Projectors.GamesProjector, as: Projector

  import Mock

  describe "it saves a new game and creates a new spotify playlist" do
    setup do
      {:ok, playlist_uuid: "3Db016Od4hWDeuNj9tmtvQ"}
    end

    test_with_mock "when valid", %{playlist_uuid: playlist_uuid}, Qewe.Qotify, [],
      create_playlist: fn _session_id, _args ->
        {:ok, playlist_uuid}
      end do
      uuid = UUID.uuid4()
      slug = "some-big-nasty-slug"

      evt = %GameCreated{game_uuid: uuid, slug: slug}

      event_num = get_last_seen_event_number("Games.Projectors.GamesProjector")

      assert :ok =
               Projector.handle(evt, %{
                 event_number: event_num + 1,
                 handler_name: "games_projector"
               })

      assert %Game{uuid: ^uuid, slug: ^slug, spotify_playlist_uuid: ^playlist_uuid} =
               Repo.get!(Game, uuid)
    end
  end
end
