defmodule Qewe.GameTest do
  use Qewe.DataCase

  alias Qewe.Games
  alias Qewe.Games.Events.GameCreated
  alias Qewe.Games.Events.SongAdded

  describe "creating games" do
    test "should create a new game" do
      slug = "some.nasty.slug"
      session_id = UUID.uuid4()
      assert {:ok, game_id} = Games.create_game(%{"slug" => slug, "session_id" => session_id})

      assert_receive_event(Qewe.App, GameCreated, fn evt ->
        assert %GameCreated{game_uuid: ^game_id, slug: ^slug, session_id: ^session_id} = evt
      end)
    end

    test "it should fail because missing data" do
      args = %{}
      assert {:error, :command_validation_failure, _cmd, msgs} = Games.create_game(args)
      refute Enum.empty?(msgs)
    end
  end

  describe "queueing songs" do
    setup do
      {:ok, game_id} = Games.create_game(%{"slug" => "test-game", "session_id" => UUID.uuid4()})

      wait_for_event(App, GameCreated)
      {:ok, %{game_id: game_id}}
    end

    test "should queue a new song", %{game_id: game_id} do
      track_id = "some-track-id"
      assert {:ok, song_id} = Games.add_song(game_id, %{"track_id" => track_id})

      assert_receive_event(App, SongAdded, fn evt ->
        assert evt.track_id == track_id
      end)
    end
  end
end
