defmodule Qewe.Games.Aggregates.GameTest do
  use Qewe.AggregateCase, aggregate: Qewe.Games.Aggregates.Game

  alias Qewe.Games.Commands.CreateGame
  alias Qewe.Games.Commands.AddSong

  alias Qewe.Games.Events.GameCreated
  alias Qewe.Games.Events.SongAdded

  describe "create_game" do
    test "it should succeed when valid" do
      game_uuid = UUID.uuid4()
      session_id = UUID.uuid4()
      slug = "some-slug-text"

      evt = %CreateGame{game_uuid: game_uuid, slug: slug, session_id: session_id}

      assert_events(evt, [
        %GameCreated{game_uuid: game_uuid, slug: "some-slug-text", session_id: session_id}
      ])

      assert_state(evt, %Qewe.Games.Aggregates.Game{
        uuid: game_uuid,
        slug: slug,
        session_id: session_id
      })
    end
  end

  describe "add song" do
    test "it should return a song added event" do
      game_uuid = UUID.uuid4()
      session_id = UUID.uuid4()
      song_id = "some-song-id"

      evt = %AddSong{game_uuid: game_uuid, session_id: session_id, track_id: song_id}

      assert_events(evt, [
        %SongAdded{game_uuid: game_uuid, track_id: song_id}
      ])
    end
  end
end
