defmodule Qewe.Support.Middleware.ValidationMiddlewareTest do
  use ExUnit.Case

  alias Commanded.Middleware.Pipeline

  alias Qewe.Support.Middleware.ValidationMiddleware, as: Middleware

  alias Qewe.Support.TestCommand

  test "it handles before_dispatch" do
    pipeline =
      %Pipeline{command: %TestCommand{value: "some-val"}}
      |> Middleware.before_dispatch()

    refute Pipeline.halted?(pipeline)
  end

  test "it fails when validation fails" do
    pipeline =
      %Pipeline{command: %TestCommand{value: "invalid"}}
      |> Middleware.before_dispatch()

    assert Pipeline.halted?(pipeline)
    assert {:error, :command_validation_failure, _, msgs} = Pipeline.response(pipeline)
    assert msgs == ["Value is invalid!"]
  end
end
